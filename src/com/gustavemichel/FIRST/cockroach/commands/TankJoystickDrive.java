
package com.gustavemichel.FIRST.cockroach.commands;

import edu.wpi.first.wpilibj.GenericHID;

/**
 *
 * @author bradmiller
 */
public class TankJoystickDrive extends CommandBase {

    public TankJoystickDrive() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(driveTrain);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        driveTrain.tankDrive(oi.joystick.getY(GenericHID.Hand.kLeft), oi.joystick.getY(GenericHID.Hand.kRight));
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
