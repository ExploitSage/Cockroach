
package com.gustavemichel.FIRST.cockroach.subsystems;

import com.gustavemichel.FIRST.cockroach.RobotMap;
import com.gustavemichel.FIRST.cockroach.commands.TankJoystickDrive;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 */
public class DriveTrain extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    private Talon leftTalon = new Talon(RobotMap.leftTalon);
    private Talon rightTalon = new Talon(RobotMap.rightTalon);
    private RobotDrive drive = new RobotDrive(leftTalon, rightTalon);
    
    public void tankDrive(double leftSpeed, double rightSpeed) {
        drive.tankDrive(leftSpeed, rightSpeed);
    }
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new TankJoystickDrive());
    }
}

